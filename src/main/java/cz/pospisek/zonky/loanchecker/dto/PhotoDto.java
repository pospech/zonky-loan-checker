package cz.pospisek.zonky.loanchecker.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class PhotoDto {

    /**
     * Name of the photo
     */
    @NotNull
    private String name;

    /**
     * Relative URL to the photo
     */
    @NotNull
    private String url;
}
