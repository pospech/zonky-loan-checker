package cz.pospisek.zonky.loanchecker.utils;

import org.springframework.core.ParameterizedTypeReference;

import java.util.List;

public class ListParameterizedTypeReference<T> extends ParameterizedTypeReference<List<T>> {
}
