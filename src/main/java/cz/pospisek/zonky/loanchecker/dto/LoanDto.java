package cz.pospisek.zonky.loanchecker.dto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cz.pospisek.zonky.loanchecker.enumeration.PurposeEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * LoanDto
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoanDto {

    /**
     * ID of the loan
     **/
    @NotNull
    private Long id;

    /**
     * The permalink for the loan  The link refers to the official Zonky website with detail of the loan.
     * It is intended to be used by 3rd party applications, which can simply use the obtained link instead of generating a custom one.
     * The main advantage of this link is guarantee that it will be valid in long term.
     **/
      private String url;

    /**
     * Name of the loan
     **/
    @NotNull
    private String name;

    /**
     * Short story of the loan. Usually some story about the purpose of a loan that attracts investors
     **/
    private String story;

    /**
     * Purpose of the loan
     **/
    @NotNull
    private PurposeEnum purpose;

    /**
     * Photos attached to this loan
     **/
    private List<PhotoDto> photos;

    /**
     * Borrower's nickname
     **/
    @NotNull
    private String nickName;

    /**
     * LoanDto term (in months)
     **/
    @NotNull
    private Short termInMonths;

    /**
     * Interest rate for investors
     **/
    @NotNull
    private BigDecimal interestRate;

    /**
     * revenue rate for investors (loan interest rate - investment fee)
     **/
    @NotNull
    private BigDecimal revenueRate;

    /**
     * loan annuity including insurance premium (if insured)
     **/
    @NotNull
    private BigDecimal annuityWithInsurance;

    /**
     * Rating of the loan  Rating is a risk category for the loan determined after scoring
     **/
    @NotNull
    private String rating;

    /**
     * true if loan has been topped, false otherwise
     **/
    @NotNull
    private Boolean topped;

    /**
     * The amount offered to and accepted by borrower
     **/
    @NotNull
    private BigDecimal amount;

    /**
     * Currency code based on ISO-4217
     **/
    @NotNull
    private String currency;

    /**
     * The remaining amount available for investment,
     * users without a reservation can invest only amount equal to 'remainingInvestment - reservedAmount'
     **/
    @NotNull
    private BigDecimal remainingInvestment;

    /**
     * The amount reserved for investors with a reservation for given loan,
     * when not zero, users without reservations can invest only amount equal to 'remainingInvestment - reservedAmount'
     **/
    @NotNull
    private BigDecimal reservedAmount;
    
    /**
     * Current investment rate
     **/
    @NotNull
    private BigDecimal investmentRate;
    
    /**
     * true if the loan is covered, false otherwise
     **/
    @NotNull
    private Boolean covered;

    /**
     * Date of the loan publication on marketplace
     **/
    @NotNull
    private ZonedDateTime datePublished;
    
    /**
     * true if the loan has been published on the marketplace, false otherwise
     **/
    @NotNull
    private Boolean published;

    /**
     * Deadline of the loan  Only loans with deadline after actual date and time are visible in the marketplace
     **/
    @NotNull
    private ZonedDateTime deadline;
    
    /**
     * The count of investments attached to this loan
     **/
    @NotNull
    private BigDecimal investmentsCount;
    /**
     * The count of questions attached to this loan
     **/
    @NotNull
    private BigDecimal questionsCount;
    
    /**
     * The code of the region
     **/
    @NotNull
    private String region;
    
    /**
     * The type of the income that is set as primary
     **/
    @NotNull
    private String mainIncomeType;
    
    /**
     * Insurance is active
     **/
    @NotNull
    private Boolean insuranceActive;
    /**
     * All insurance intervals
     */
    @NotNull
    private List<InsuranceHistoryItemDto> insuranceHistory = new ArrayList<>();

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof LoanDto)) {
            return false;
        }
        LoanDto loanDto = (LoanDto) obj;
        return id.equals(loanDto.id);
    }

    @Override
    public String toString() {
        return String.format("Name: %s; Published: %s; Term: %o months; Interest rate: %s, Revenue rate: %s; Rating: %s, Detail: %s",
                name, datePublished, termInMonths, interestRate != null ? interestRate.toString() : "",
                revenueRate != null ? revenueRate.toString() : "", rating, url);
    }
}