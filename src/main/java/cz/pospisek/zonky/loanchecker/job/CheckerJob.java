package cz.pospisek.zonky.loanchecker.job;

import cz.pospisek.zonky.loanchecker.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Single class bean which wraps scheduled operations.
 */
@Component
public class CheckerJob {

    private final LoanService loanService;

    @Autowired
    public CheckerJob(LoanService loanService) {
        this.loanService = loanService;
    }

    /**
     * Sheduled job for checking new loans in Zonky's marketplace
     */
    @Scheduled(fixedDelayString = "${jobs.checkers.newLoans.fixedRate}")
    public void checkNewLoans() {
        loanService.checkAndOutputNewLoans();
    }
}
