package cz.pospisek.zonky.loanchecker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Basic Spring Boot entry point of application.
 */
@SpringBootApplication
@EnableScheduling
public class CheckerBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(CheckerBootApplication.class, args);
    }
}
