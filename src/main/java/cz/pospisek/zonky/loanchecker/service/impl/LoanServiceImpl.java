package cz.pospisek.zonky.loanchecker.service.impl;

import cz.pospisek.zonky.loanchecker.dto.LoanDto;
import cz.pospisek.zonky.loanchecker.service.LoanService;
import cz.pospisek.zonky.loanchecker.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class LoanServiceImpl implements LoanService {

    private Set<Long> printedIds = new HashSet<>();

    private final RestTemplate restTemplate;

    /**
     * Starting point from which new loans are loaded
     */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @Value("${jobs.checkers.newLoans.startingDate}")
    private ZonedDateTime startingPoint;

    /**
     * Size of Zonky's marketplace result. By default 20
     */
    @Value("${jobs.checkers.newLoans.pageSize}")
    private int pageSize;

    @Autowired
    public LoanServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public void checkAndOutputNewLoans() {
        /*
         * First step before call of rest endpoint of zonky, appropriate headers are created.
         * User agent is required according to Zonky's API.
         * User agent has template like this: ${application}/${version} (${url})
         * X-Order header is used for sorting.
         * Multiple fields are separated by comma, default ordering is ASC, if DSC is required, then minus needs to be set as field prefix
         * */
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.USER_AGENT, Constants.USER_AGENT_VALUE);
        headers.set("X-Order", "datePublished");
        headers.set("X-Size", String.valueOf(pageSize));
        HttpEntity requestEntity = new HttpEntity(headers);


        /*
         * Next there is load function called to get all the new loans from specified date
         */
        List<LoanDto> newLoans = loadLoansFromDate(startingPoint, requestEntity);

        /*
         * Finally, if there are some new loans, then they are added to cache and printed.
         * If there are no new loans, then sad message is printed.
         */
        if (!newLoans.isEmpty()) {
            log.info("There are {} new loans", newLoans.size());
            newLoans.forEach(loanDto -> log.info(loanDto.toString()));
        } else {
            log.info("There are no new loans");
        }
    }

    /**
     * Function for loading new loans on Zonky's marketplace.
     * Initially, from date is taken from application properties, where it is stored as starting point from which loans should be loaded.
     * Then, bulk of default 20 new loans from this specified date are loaded.
     * Their id is added to already loaded set of IDs.
     * If the bulk has size of default 20, then date from newest one is used in recursive call as this starting date and results are appended
     * If bulk has less than default 20 loans, then starting point date is set to the date of newest loan.
     *
     * @param fromDate Date, from hich loans should be leaded
     * @param requestEntity Data required for REST call of Zonky's marketplace
     * @return List of all new loans on Zonky's marketplace
     */
    private List<LoanDto> loadLoansFromDate(ZonedDateTime fromDate, HttpEntity requestEntity) {
        try {
            /*
            * Creation of URI for marketplace REST call.
            * Contains filtering by datePublished field.
            * */
            URI uri = new URIBuilder(Constants.MARKETPLACE_URL)
                    .addParameter("datePublished__gte", fromDate.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))
                    .build();

            /*
            * REST call to marketplace
            * */
            LoanDto[] loansArray = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, LoanDto[].class).getBody();

            /*
            * If there were no new loans, then empty list is returned
            * */
            if(loansArray.length == 0) {
                return Collections.emptyList();
            }

            /*
            * Filter of data from marketplace for only those, which we already don't have
            * */
            List<LoanDto> result = Arrays.stream(loansArray).filter(loan -> !printedIds.contains(loan.getId())).collect(Collectors.toList());

            /*
             * If there were no new loans, then empty list is returned
             * */
            if(result.isEmpty()) {
                return Collections.emptyList();
            }

            /*
            * Adding new loan ids to cache
            * */
            printedIds.addAll(result.stream().map(LoanDto::getId).collect(Collectors.toSet()));

            /*
            * Starting point date is set to be the date of newest loan publishedDate
            * */
            LoanDto lastLoan = result.get(result.size() - 1);
            startingPoint = lastLoan.getDatePublished();

            /*
            * If there is default 20 loans in REST response from market place it means, that there are still some newer loans.
            * Result is enriched with recursive call to loading data from marketplace.
            * */
            if (loansArray.length == pageSize) {
                result.addAll(loadLoansFromDate(startingPoint, requestEntity));
            }

            return result;
        } catch (URISyntaxException e) {
            log.error("Wrong syntax of uri during it's creation", e);
            return Collections.emptyList();
        }
    }
}
