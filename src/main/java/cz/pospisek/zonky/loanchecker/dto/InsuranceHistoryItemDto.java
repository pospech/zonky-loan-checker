package cz.pospisek.zonky.loanchecker.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * InsuranceHistoryItemDto
 */
@Getter
@Setter
@NoArgsConstructor
class InsuranceHistoryItemDto {

    /**
     * Get policyPeriodFrom
     **/
    @JsonProperty("policyPeriodFrom")
    @NotNull
    private LocalDate policyPeriodFrom = null;

    /**
     * Get policyPeriodTo
     **/
    @JsonProperty("policyPeriodTo")
    @NotNull
    private LocalDate policyPeriodTo = null;

}

