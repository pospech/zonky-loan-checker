package cz.pospisek.zonky.loanchecker.utils;

/**
 * Wrapper of static constant values
 */
public class Constants {

    public static final String MARKETPLACE_URL = "https://api.zonky.cz/loans/marketplace";
    public static final String USER_AGENT_VALUE = "zonky-loan-checker/1.0 (https://gitlab.com/pospech/zonky-loan-checker)";
}
