# Zonky's loan checker

Simple spring boot app for checking new loans on Zonky's marketplace.
Zonky has public REST API from which loans can be obtained.
Currently, basic bulk of 20 newest loans is loaded.
Those loans are then filtered for those which were not stored into memory cache.
Those  If there are some loans like this they are stored into cache and printed to output.

## Used technologies
* Java 1.8
* Spring Boot
* Gradle 