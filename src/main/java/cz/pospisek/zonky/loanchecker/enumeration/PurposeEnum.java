package cz.pospisek.zonky.loanchecker.enumeration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Purpose of the loan
 */
public enum PurposeEnum {
    AUTO_MOTO("AUTO_MOTO"),

    EDUCATION("EDUCATION"),

    TRAVEL("TRAVEL"),

    ELECTRONICS("ELECTRONICS"),

    HEALTH("HEALTH"),

    REFINANCING("REFINANCING"),

    HOUSEHOLD("HOUSEHOLD"),

    OWN_PROJECT("OWN_PROJECT"),

    OTHER("OTHER");

    private String value;

    PurposeEnum(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static PurposeEnum fromValue(String text) {
        for (PurposeEnum b : PurposeEnum.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        return null;
    }
}