package cz.pospisek.zonky.loanchecker.service;

/**
 * Service dedicated to work with loans of Zonky's marketplace
 */
public interface LoanService {

    /**
     * Function is supposed to check if there are new loans in Zonky's marketplace and if there are some, output them.
     * Zonky's marketplace is using standard REST API, which is documented in Zonky's API DOC
     * @see <a href="https://zonky.docs.apiary.io">API DOC</a>
     */
    void checkAndOutputNewLoans();
}
